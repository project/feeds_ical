# Feeds iCal

This module provides a Feeds Parser and Feeds Item to allow for the import of content from iCal sources.

The module relies heavily on the [johngrogg/ics-parser][] package which it requires.

Common extensions for iCalendar files are .ical, .ics, .ifb,
and .icalendar.


## Usage

Create a new Feed Type that uses the "Ical Parser" parser.
When editing your mappings, you will see the predefined fields
expected from an calendar feed.

### Date and Time sources
The fields DTSTART and DTEND are automatically converted into UNIX
timestamps as most use cases will involve mapping these to a
Date/Time field of the target entity type.

In the case where the user desires the raw values for these fields,
the two fields DTSTART_RAW and DTEND_RAW have been added as of 
release 2.3.0.

## Please Note

This module does not as of yet deal with RRULE occurrences, though the underlying library does,
 so it would be a matter of adding that functionality to the Feeds Parser.


 [johngrogg/ics-parser]: https://packagist.org/packages/johngrogg/ics-parser
