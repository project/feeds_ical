<?php

declare(strict_types=1);

namespace Drupal\feeds_ical\Feeds\Parser;

use Drupal\feeds\Component\XmlParserTrait;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Parser\ParserInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds_ical\Feeds\Item\IcalItem;
use ICal\ICal;

/**
 * Defines an Ical Feeds parser.
 *
 * @FeedsParser(
 *   id = "feeds_ical",
 *   title = @Translation("Ical Parser"),
 *   description = @Translation("Parse Ical Feed.")
 * )
 */
class IcalParser extends PluginBase implements ParserInterface {
  use XmlParserTrait;

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state): ParserResult {

    // Look at our response.
    $rawResponseString = trim($fetcher_result->getRaw());
    if (empty($rawResponseString)) {
      throw new EmptyFeedException();
    }

    // Create our result object to add to.
    $result = new ParserResult();

    // Use our ical class to parse the feed.
    try {

      // Create ical parsing object.
      $ical = new ICal('ICal.ics', [
      // Default value.
        'defaultSpan'                 => 2,
        'defaultTimeZone'             => 'UTC',
      // Default value.
        'defaultWeekStart'            => 'MO',
      // Default value.
        'disableCharacterReplacement' => FALSE,
      // Default value.
        'skipRecurrence'              => FALSE,
      // Default value.
        'useTimeZoneWithRRules'       => FALSE,
      ]);
      $ical->initString($rawResponseString);

      // Get and loop over events.
      $events = $ical->events();
      foreach ($events as $eventIndex => $event) {

        // Create item to return for processing.
        $itemObject = new IcalItem();
        // Loop over information from feed and add to item.
        foreach ($event as $eventProperty => $eventPropertyValue) {

          switch ($eventProperty) {
            case 'dtstart':
              $itemObject->set($eventProperty, $event->dtstart_array[2]);

              // Save the original value in the raw source field.
              $itemObject->set('dtstart_raw', $eventPropertyValue);
              break;

            case 'dtend':
              if (!empty($event->dtend_array[2])) {
                $itemObject->set($eventProperty, $event->dtend_array[2]);

                // Save the original value in the raw source field.
                $itemObject->set('dtend_raw', $eventPropertyValue);
              }
              break;

            default:
              $itemObject->set($eventProperty, $eventPropertyValue);

          }

        }

        // Loop over any additional properties and add to item.
        foreach ($event->additionalProperties as $eventProperty => $eventPropertyValue) {

          // Skip the "_array" fields that ICal adds.
          if (strpos($eventProperty, '_array') !== FALSE) {
            continue;
          }
          $itemObject->set($eventProperty, $eventPropertyValue);
        }

        // Add item to results.
        $result->addItem($itemObject);

      }

    }
    catch (\Exception $e) {
      \Drupal::logger('feeds_ical')->error($e);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources(): array {
    return [
      'dtstart' => [
        'label' => $this->t('DTSTART (unix timestamp)'),
      ],
      'dtend' => [
        'label' => $this->t('DTEND (unix timestamp)'),
      ],
      'dtstart_raw' => [
        'label' => $this->t('DTSTART RAW (string format from source)'),
      ],
      'dtend_raw' => [
        'label' => $this->t('DTEND RAW (string format from source)'),
      ],
      'dtstamp' => [
        'label' => $this->t('DTSTAMP'),
      ],
      'rrule' => [
        'label' => $this->t('RRULE'),
      ],
      'uid' => [
        'label' => $this->t('UID'),
        'suggestions' => [
          'targets' => ['guid'],
        ],
      ],
      'created' => [
        'label' => $this->t('CREATED'),
      ],
      'description' => [
        'label' => $this->t('DESCRIPTION'),
      ],
      'lastmodified' => [
        'label' => $this->t('LAST-MODIFIED'),
      ],
      'location' => [
        'label' => $this->t('LOCATION'),
      ],
      'sequence' => [
        'label' => $this->t('SEQUENCE'),
      ],
      'status' => [
        'label' => $this->t('STATUS'),
      ],
      'summary' => [
        'label' => $this->t('SUMMARY'),
        'suggestions' => [
          'targets' => ['title'],
        ],
      ],
      'transp' => [
        'label' => $this->t('TRANSP'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedCustomSourcePlugins(): array {
    return ['feeds_ical'];
  }

}
