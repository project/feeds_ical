<?php

namespace Drupal\feeds_ical\Feeds\CustomSource;

use Drupal\feeds\Feeds\CustomSource\BlankSource;

/**
 * A CSV source.
 *
 * @FeedsCustomSource(
 *   id = "feeds_ical",
 *   title = @Translation("Ical"),
 * )
 */
class IcalSource extends BlankSource {
}
