<?php

namespace Drupal\feeds_ical\Feeds\Item;

use Drupal\feeds\Feeds\Item\BaseItem;

/**
 * Defines an item class for use with an Ical document.
 *
 * @package Drupal\feeds_ical\Feeds\Item
 */
class IcalItem extends BaseItem {

  /**
   * The start date and time for the event.
   *
   * @var mixed
   */
  protected $dtstart;

  /**
   * The end date and time for the event.
   *
   * @var mixed
   */
  protected $dtend;

  /**
   * The raw string value of the start date and time from the feed.
   *
   * @var mixed
   */
  protected $dtstart_raw;

  /**
   * The raw string value of the end date and time from the feed.
   *
   * @var mixed
   */
  protected $dtend_raw;

  /**
   * The date and time the event was created.
   *
   * @var mixed
   */
  protected $dtstamp;

  /**
   * The unique identifier for the event.
   *
   * @var mixed
   */
  protected $uid;

  /**
   * The date and time the event was created.
   *
   * @var mixed
   */
  protected $created;

  /**
   * The description of the event.
   *
   * @var mixed
   */
  protected $description;

  /**
   * The last modified date and time for the event.
   *
   * @var mixed
   */
  protected $lastmodified;

  /**
   * The location of the event.
   *
   * @var mixed
   */
  protected $location;

  /**
   * The sequence number of the event.
   *
   * @var mixed
   */
  protected $sequence;

  /**
   * The status of the event.
   *
   * @var mixed
   */
  protected $status;

  /**
   * The summary of the event.
   *
   * @var mixed
   */
  protected $summary;

  /**
   * The transparency of the event.
   *
   * @var mixed
   */
  protected $transp;

  /**
   * The organizer of the event.
   *
   * @var mixed
   */
  protected $organizer;

  /**
   * The attendee of the event.
   *
   * @var mixed
   */
  protected $attendee;

  /**
   * Additional properties of the event.
   *
   * @var mixed
   */
  protected  $additionalProperties;

  /**
   * The duration of the event.
   *
   * @var mixed
   */
  protected  $duration;

  /**
   * The end time zone of the event.
   *
   * @var mixed
   */
  protected  $dtend_tz;

  /**
   * The start time zone of the event.
   *
   * @var mixed
   */
  protected  $dtstart_tz;

  /**
   * The last modified date and time for the event.
   *
   * @var mixed
   */
  protected $last_modified;

}
